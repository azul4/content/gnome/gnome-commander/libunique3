# libunique3

Library for writing single instance applications for GTK3

https://wiki.gnome.org/Attic/LibUnique

http://ftp.gnome.org/pub/gnome/sources/libunique

<br>

How to clone this repository:

```
git clone https://gitlab.com/azul4/content/gnome/gnome-commander/libunique3.git
```

